import sys

def suma(primero ,segundo):
    if type(primero) != int or type(segundo) != int:
        sys.exit("invalid operation")
    return primero +  segundo

def resta(primero ,segundo):
    if type(primero) != int or type(segundo) != int:
        sys.exit("invalid operation")
    return primero -  segundo

def operacion(argumento, num1, num2):
    if argumento == 'suma':
        print(f'El resultado de la suma es : {suma(int(num1),int(num2))}')  # son 1 y 2 por qeu el argumento 0 de sys.argv es el nombre del program

    elif argumento == 'resta':
        print(f'El resultado de la resta es : {resta(int(num1), int(num2))}') # son 1 y 2 por qeu el argumento 0 de sys.argv es el nombre del programa

    else:
        sys.exit("Por favor revisa los agumentos, debes poner suma o resta seguido de 2 numeros ")


if __name__ == '__main__':
    argumentos = sys.argv

    operacion(argumentos[1] , argumentos[2], argumentos[3])
    operacion(argumentos[4] , argumentos[5], argumentos[6])
    operacion(argumentos[7] , argumentos[8], argumentos[9])
    operacion(argumentos[10] , argumentos[11], argumentos[12])




